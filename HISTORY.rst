=======
History
=======

0.1.1 (2021-01-16)
------------------

* Package metadata fixes
* [Doc] github -> gitlab

0.1.0 (2021-01-14)
------------------

* First release on PyPI.
