import math

from fastidious2.reader import Reader
from fastidious2.compiler import compile_peg
from fastidious2.ast.sexpr import sexpr

from fastidious2.grammar.peg.bootstrap import parser as peg

GRAMMAR = r"""
grammar <- result:expression EOF { result }
digit <- [0-9]
nz <- [1-9]
_ <- [ \n\t\r]*
integer <- $( nz digit* )
EOF <- !.
expression <-
  |--
  | (@) _ "+" _ @ { lhs + rhs }
  | (@) _ "-" _ @ { lhs - rhs }
  |--
  | (@) _ "*" _ @ { lhs * rhs }
  | (@) _ "/" _ @ { lhs / rhs }
  |--
  | @ _ "^" _ (@) { lhs ** rhs }
  |--
  | _ "-" _ @ { -x }
  |--
  | @ _ "!" _ { math.factorial(x) }
  |--
  | i:integer { int(str(i)) }
  | "(" _ e:expression _ ")" { e }
"""

calc = peg(Reader(GRAMMAR))
ns = {"math": math}
compile_peg(calc, ns=ns)


def run(expr):
    r = Reader(expr)
    return calc(r)


if __name__ == "__main__":
    import sys
    if len(sys.argv) == 2:
        expr = sys.argv[1]
    else:
        expr = "-2 * (2 + 3)!"
    print(run(expr))
