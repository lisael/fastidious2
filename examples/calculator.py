import math

from fastidious2.reader import Reader
from fastidious2.expression import (
        Literal, AnyChar, Seq, Repeat, Not, CharRange,
        Action, Label, RuleSet, Rule, RuleDef,
        LeftAssocInfix, RightAssocInfix, Prefix, Postfix, Slice,
        PrecedenceRule
        )
from fastidious2.compiler import compile_peg
from fastidious2.ast.sexpr import sexpr


parser = RuleSet(
    # digit <- [0-9]
    RuleDef(
        "digit",
        CharRange("", ranges=(["0", "9"],))),
    # nz <- [1-9]
    RuleDef(
        "nz",
        CharRange("", ranges=(["1", "9"],))),
    # integer <- nz digit*
    RuleDef(
        "integer",
        Slice(
            Seq(
                Rule("nz"),
                Repeat(Rule("digit"))))),
    # _ <- [ \n\t\r]*
    RuleDef(
        "_",
        Repeat(CharRange(" \n\t\r"))),
    # EOF <- !.
    RuleDef(
        "EOF",
        Not(AnyChar())),
    # addOp
    RuleDef(
        "addOp",
        Seq(
            Rule("_"),
            Literal("+"),
            Rule("_"))),
    # minusOp
    RuleDef(
        "minusOp",
        Seq(
            Rule("_"),
            Literal("-"),
            Rule("_"))),
    # multOp
    RuleDef(
        "multOp",
        Seq(
            Rule("_"),
            Literal("*"),
            Rule("_"))),
    # divOp
    RuleDef(
        "divOp",
        Seq(
            Rule("_"),
            Literal("/"),
            Rule("_"))),
    # expOp
    RuleDef(
        "expOp",
        Seq(
            Rule("_"),
            Literal("^"),
            Rule("_"))),
    # factOp
    RuleDef(
        "factOp",
        Seq(
            Rule("_"),
            Literal("!"),
            Rule("_"))),
    PrecedenceRule(
        "expression",
        LeftAssocInfix(
            None,
            (
                (Rule("addOp"), 'lhs + rhs'),
                (Rule("minusOp"), 'lhs - rhs'),
            )),
        LeftAssocInfix(
            None,
            (
                (Rule("multOp"), 'lhs * rhs'),
                (Rule("divOp"), 'lhs / rhs'),
            )),
        RightAssocInfix(
            None,
            (
                (Rule("expOp"), 'lhs ** rhs'),
            )),
        Prefix(
            None,
            (
                (Rule("minusOp"), '-x'),
            )),
        Postfix(
            None,
            (
                (Rule("factOp"), 'math.factorial(x)'),
            )),
        Action(
            Label(
                "i",
                Rule("integer")),
            "int(str(i))"),
        Action(
            Seq(
                Literal("("), Rule("_"),
                Label("e", Rule("expression")),
                Rule("_"), Literal(")")),
            "e")),
    entrypoint="expression"
)

# print(sexpr(parser))
sexpr(parser)

print(parser.as_grammar())

ns = {"math": math}
compile_peg(parser, ns=ns)


def run(expr):
    r = Reader(expr)
    return parser(r)


if __name__ == "__main__":
    import sys
    if len(sys.argv) == 2:
        expr = sys.argv[1]
    else:
        expr = "(--2 + 3!!) * 2! + (2 / 5^2^(2 * 3))"
    print(run(expr))
