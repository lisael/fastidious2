.. highlight:: shell

============
Installation
============


Stable release
--------------

To install fastidious2, run this command in your terminal:

.. code-block:: console

    $ pip install fastidious2

This is the preferred method to install fastidious2, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From sources
------------

The sources for fastidious2 can be downloaded from the `Gitlab repo`_.

You can either clone the public repository:

.. code-block:: console

    $ git clone git://gitlab.com/lisael/fastidious2

Or download the `tarball`_:

.. code-block:: console

    $ curl -OJL https://gitlab.com/lisael/fastidious2/-/archive/master/fastidious2-master.tar.gz

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ python setup.py install


.. _Gitlab repo: https://gitlab.com/lisael/fastidious2
.. _tarball: https://gitlab.com/lisael/fastidious2/-/archive/master/fastidious2-master.tar.gz
