===========
fastidious2
===========


.. image:: https://img.shields.io/pypi/v/fastidious2.svg
        :target: https://pypi.python.org/pypi/fastidious2

.. image:: https://gitlab.com/lisael/fastidious2/badges/master/pipeline.svg
        :target: https://gitlab.com/lisael/fastidious2/-/pipelines

.. image:: https://gitlab.com/lisael/fastidious2/badges/master/coverage.svg
        :target: https://gitlab.com/lisael/fastidious2/-/pipelines

.. image:: https://readthedocs.org/projects/fastidious2/badge/?version=latest
        :target: https://fastidious2.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




The best PEG parser generator I write (so far)


* Free software: GNU General Public License v3
* Documentation: https://fastidious2.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
