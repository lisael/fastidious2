def test_calculator():
    import sys
    sys.path.append(".")
    from examples.calculator import run
    assert run("3+2") == 5


def test_calculator_peg():
    import sys
    sys.path.append(".")
    from examples.calculator_peg import run
    assert run("3+2") == 5
    assert run("-2 * (2 + 3)!") == -240
