from fastidious2.reader import Reader


def test_runes():
    r = Reader("ab")
    assert bytes(r.next_rune()) == b"a"
    r = Reader("œb")
    assert bytes(r.next_rune()) == "œ".encode("utf-8")
    r = Reader("œ")
    assert bytes(r.next_rune()) == "œ".encode("utf-8")
    r = Reader("œœ")
    assert bytes(r.next_rune()) == "œ".encode("utf-8")
