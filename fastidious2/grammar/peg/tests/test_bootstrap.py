import textwrap as tw
from fastidious2.ast.sexpr import sexpr
from fastidious2.reader import Reader

from fastidious2.expression import (
        Literal, AnyChar, Seq, Choice, OneOrMore,
        Repeat, Optional, LookAhead, Not, CharRange,
        Action, Label, RuleSet, Rule, RuleDef, ActionFunc,
        LeftAssocInfix, RightAssocInfix, Prefix, Postfix, Slice,
        PrecedenceRule, Concat, Discard
        )

from ..bootstrap import parser, ns
from fastidious2.compiler import compile_peg


def test_identifier():
    result = parser(Reader("ident foo"), entrypoint="identifier")
    assert str(result) == "ident"


def test_rule():
    src = tw.dedent("""\
        rule <- . lab:other_rule""")
    result = parser(Reader(src), entrypoint="rule")
    assert result == RuleDef(
            "rule",
            Seq(
                AnyChar(),
                Label(
                    "lab",
                    Rule("other_rule"))))
    src = "rule <- !(.)"
    result = parser(Reader(src), entrypoint="rule")
    assert result == RuleDef(
            "rule",
            Not(AnyChar()))


def test_literal():
    src = r"'ab'"
    result = parser(Reader(src), entrypoint="lit_expr")
    assert result == Literal("ab")
    src = r"'a\'b'"
    result = parser(Reader(src), entrypoint="lit_expr")
    assert result == Literal("a'b")
    src = r"'a\nb'"
    result = parser(Reader(src), entrypoint="lit_expr")
    assert result == Literal("a\nb")


def test_prefixes():
    src = "!."
    result = parser(Reader(src), entrypoint="expression")
    assert result == Not(AnyChar())
    src = "!( . )"
    result = parser(Reader(src), entrypoint="expression")
    assert result == Not(AnyChar())
    src = "&."
    result = parser(Reader(src), entrypoint="expression")
    assert result == LookAhead(AnyChar())
    src = r"\."
    result = parser(Reader(src), entrypoint="expression")
    assert result == Discard(AnyChar())
    src = "$."
    result = parser(Reader(src), entrypoint="expression")
    assert result == Slice(AnyChar())
    src = "$$."
    result = parser(Reader(src), entrypoint="expression")
    assert result == Concat(AnyChar())


def test_suffixes():
    src = ".*"
    result = parser(Reader(src), entrypoint="expression")
    assert result == Repeat(AnyChar())


def test_char_class():
    src = "[ab]"
    result = parser(Reader(src), entrypoint="char_range_expr")
    assert result == CharRange([b"a", b"b"], [])
    src = "[a-z]"
    result = parser(Reader(src), entrypoint="char_range_expr")
    assert result == CharRange([], [["a", "z"]])
    src = "[-a-z0-9]"
    result = parser(Reader(src), entrypoint="char_range_expr")
    assert result == CharRange([b"-"], [["a", "z"], ["0", "9"]])
    src = r"[\b\r\n\t\\[\]]"
    result = parser(Reader(src), entrypoint="char_range_expr")
    assert result == CharRange(
            [b"\b", b"\r", b"\n", b"\t", b"\\", b"[", b"]"], [])


def test_self_compiling():
    # compile the parser's grammar with itself
    src = parser.as_grammar()
    result = parser(Reader(src))
    compile_peg(result, ns=ns)
    assert sexpr(result) == sexpr(parser)

    # compile the preceding resulting parser's grammar with itself
    src = result.as_grammar()
    result2 = result(Reader(src))
    compile_peg(result2, ns=ns)
    assert sexpr(result2) == sexpr(parser)
    assert sexpr(result2) == sexpr(result)
