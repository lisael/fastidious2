import textwrap as tw
from functools import reduce

from fastidious2.reader import Reader
from fastidious2.expression import (
        Literal, AnyChar, Seq, Choice, OneOrMore,
        Repeat, Optional, LookAhead, Not, CharRange,
        Action, Label, RuleSet, Rule, RuleDef, ActionFunc,
        LeftAssocInfix, RightAssocInfix, Prefix, Postfix, Slice,
        PrecedenceRule, Concat, Discard, Breakpoint
        )
from fastidious2.compiler import compile_peg


bootstrapper = RuleSet(
    # grammar <- __ rules:( rule \__ )+ EOF { RuleSet(rules) }
    RuleDef(
        "grammar",
        Action(
            Seq(
                Rule("__"),
                Label(
                    "rules",
                    OneOrMore(
                        Seq(
                            Rule("rule"),
                            Discard(Rule("__"))))),
                Rule("EOF")),
            "RuleSet(*[r[0] for r in rules])")),
    # rule <- name:identifier __ "<-" __ expr:expression EOS { RuleDef(str(name), expr)}
    RuleDef(
        "rule",
        Action(
            Seq(
                Label(
                    "name",
                    Rule("identifier")),
                Rule("__"),
                Literal("<-"),
                Rule("__"),
                Label(
                    "expr",
                    Rule("expression")),
                Rule("EOS")),
            "RuleDef(str(name), expr)")),
    # expression <- choice_expr
    RuleDef(
        "expression",
        Rule("choice_expr")),
    # choice_expr <- first:seq_expr rest:( \__ \"/" \__ seq_expr )* {
    #     if not rest:
    #         return first
    #     else:
    #         return Choice([first] + rest)
    RuleDef(
        "choice_expr",
        Action(
            Seq(
                Label(
                    "first",
                    Rule("seq_expr")),
                Label(
                    "rest",
                    Repeat(
                        Seq(
                            Discard(Rule("__")),
                            Discard(Literal("/")),
                            Discard(Rule("__")),
                            Rule("seq_expr"))))),
            tw.dedent("""\
                if not rest:
                    return first
                else:
                    parts = [first] + [r[0] for r in rest]
                    return Choice(*parts)"""))),
    # seq_expr <-
    #     first:labeled_expr
    #     rest:( __ labeled_expr )*
    #     ( __ action:code_block)? {
    #         if not rest:
    #             expr = first
    #         else:
    #             expr = Seq([first] + rest)
    #         if action:
    #             return Action(expr, action)
    #         else:
    #             return expr}
    RuleDef(
        "seq_expr",
        Action(
            Seq(
                Label(
                    "first",
                    Rule("labeled_expr")),
                Label(
                    "rest",
                    Repeat(
                        Seq(
                            Discard(Rule("__")),
                            Rule("labeled_expr")))),
                Optional(
                    Seq(
                        Rule("__"),
                        Label(
                            "action",
                            Rule("code_block"))))),
            tw.dedent("""\
                if not rest:
                    expr = first
                else:
                    parts = [first] + [r[0] for r in rest]
                    expr = Seq(*parts)
                if action:
                    return Action(expr, tw.dedent(action))
                else:
                    return expr"""))),
    # code_block <- "{" code:code "}" { code }
    RuleDef(
        "code_block",
        Action(
            Seq(
                Literal("{"),
                Label(
                    "code",
                    Rule("code")),
                Literal("}")),
            "code")),
    # code <-
    #     code:$(
    #         (
    #           ( ![{}] source_char )+
    #           / "{" code "}"
    #         )*
    #     ) {str(code).strip()}
    RuleDef(
        "code",
        Action(
            Label(
                "code",
                Slice(
                    Repeat(
                        Choice(
                            OneOrMore(
                                Seq(
                                    Not(
                                        CharRange("{}")),
                                    AnyChar())),
                            Seq(
                                Literal("{"),
                                Rule("code"),
                                Literal("}")))))),
            "str(code)")),
    # labeled_expr <-
    #     label:$$(identifier -__ -':' -__)? expr:prefixed_expr {
    #         if label:
    #             return Label(label, expr)
    #         else:
    #             return expr}
    RuleDef(
        "labeled_expr",
        Action(
            Seq(
                Label(
                    "label",
                    Optional(
                        Concat(
                            Seq(
                                Rule("identifier"),
                                Discard(Rule("__")),
                                Discard(Literal(":")),
                                Discard(Rule("__")))))),
                Label(
                    "expr",
                    Rule("prefixed_expr"))),
            tw.dedent("""\
                if label:
                    return Label(str(label), expr)
                else:
                    return expr"""))),
    # prefixed_expr <- prefixes:( $$(prefix -__) )* expr:suffixed_expr {
    #     prefix_classes = {
    #         "\\": Discard,
    #         "!": Not,
    #         "&": LookAhead,
    #         "$": Slice,
    #         "$$": Concat,
    #     }
    #     for p in reversed(prefixes):
    #         expr = prefix_classes[str(p)](expr)}
    RuleDef(
        "prefixed_expr",
        Action(
            Seq(
                Label(
                    "prefixes",
                    Repeat(
                        Concat(
                            Seq(
                                Rule("prefix"),
                                Discard(
                                    Rule("__")))))),
                Label(
                    "expr",
                    Rule("suffixed_expr"))),
            tw.dedent("""\
                prefix_classes = {
                    "\\\\": Discard,
                    "!": Not,
                    "&": LookAhead,
                    "$": Slice,
                    "$$": Concat,
                }
                for p in reversed(prefixes):
                    expr = prefix_classes[str(p)](expr)
                expr"""))),
    # suffixed_expr <- expr:primary_expr suffix:( $$( -__ suffix ) )?
    RuleDef(
        "suffixed_expr",
        Action(
            Seq(
                Label(
                    "expr",
                    Rule("primary_expr")),
                Label(
                    "suffix",
                    Optional(
                        Concat(
                            Seq(
                                Discard(Rule("__")),
                                Rule("suffix")))))),
            tw.dedent("""\
                if not suffix:
                    return expr
                suffix_classes = {
                    "?": Optional,
                    "+": OneOrMore,
                    "*": Repeat,
                }
                return suffix_classes[str(suffix)](expr)"""))),
    # suffix <- [?+*]
    RuleDef(
        "suffix",
        CharRange("?+*")),
    # prefix <- "$$" / [\\$!&]
    RuleDef(
        "prefix",
        Choice(
            Literal("$$"),
            CharRange("\\$!&"))),
    # primary_expr <-
    #     lit_expr
    #     / char_range_expr
    #     / any_char_expr
    #     / rule_expr
    #     / SemanticPredExpr
    #     / sub_expr
    RuleDef(
        "primary_expr",
        Choice(
            Rule("rule_expr"),
            Rule("lit_expr"),
            Rule("char_range_expr"),
            Rule("any_char_expr"),
            Rule("sub_expr")
            )),
    # sub_expr <- "(" __ expr:expression __ ")" { expr }
    RuleDef(
        "sub_expr",
        Action(
            Seq(
                Literal("("),
                Rule("__"),
                Label(
                    "expr",
                    Rule("expression")),
                Rule("__"),
                Literal(")")),
            "expr")),

    # lit_expr <- lit:string_literal ignore:"i"? {
    #     Literal(bytes(lit), ignorecase=bool(ignore))}
    RuleDef(
        "lit_expr",
        Action(
            Seq(
                Label(
                    "lit",
                    Rule("string_literal")),
                Label(
                    "ignore",
                    Optional(Literal("i")))),
            "Literal(bytes(lit), ignorecase=bool(ignore))")),
    # string_literal <-
    #     '"' content:$$(double_string_char*) '"'
    #     / "'" content:$$(single_string_char*) "'" { content }
    RuleDef(
        "string_literal",
        Action(
            Choice(
                Seq(
                    Literal('"'),
                    Label(
                        "content",
                        Concat(Repeat(Rule("double_string_char")))),
                    Literal('"')),
                Seq(
                    Literal("'"),
                    Label(
                        "content",
                        Concat(Repeat(Rule("single_string_char")))),
                    Literal("'"))),
            "content")),
    # double_string_char <-
    #     (!( '"' / "\\" / EOL ) char:.
    #     / "\\" char:double_string_escape) { char }
    RuleDef(
        "double_string_char",
        Action(
            Choice(
                Seq(
                    Not(
                        Choice(
                            Literal('"'),
                            Literal("\\"),
                            Rule("EOL"))),
                    Label(
                        "char",
                        AnyChar())),
                Seq(
                    Literal("\\"),
                    Label(
                        "char",
                        Rule("double_string_escape")))),
            "char")),
    # single_string_char <-
    #     (!( '"' / "\\" / EOL ) char:.
    #     / "\\" char:single_string_escape) { char }
    RuleDef(
        "single_string_char",
        Action(
            Choice(
                Seq(
                    Not(
                        Choice(
                            Literal("'"),
                            Literal("\\"),
                            Rule("EOL"))),
                    Label(
                        "char",
                        AnyChar())),
                Seq(
                    Literal("\\"),
                    Label(
                        "char",
                        Rule("single_string_escape")))),
            "char")),

    # single_string_escape <- "'" {b"'"} / common_escape
    RuleDef(
        "single_string_escape",
        Choice(
            Action(
                Literal("'"),
                'b"\'"'),
            Rule("common_escape"))),

    # double_string_escape <- '"' {b'"'} / common_escape
    RuleDef(
        "double_string_escape",
        Choice(
            Action(
                Literal('"'),
                "b'\"'"),
            Rule("common_escape"))),

    # common_escape <-
    #    single_char_escape
    #    / OctalEscape
    #    / HexEscape
    #    / LongUnicodeEscape
    #    / ShortUnicodeEscape
    RuleDef(
        "common_escape",
        Rule("single_char_escape")),
    # single_char_escape <-
    #     'a' {'\a'}
    #     \ 'b' {'\b'}
    #     \ 'n' {'\n'}
    #     \ 'f' {'\f'}
    #     \ 'r' {'\r'}
    #     \ 't' {'\t'}
    #     \ 'v' {'\v'}
    #     \ '\\' {'\\'}
    RuleDef(
        "single_char_escape",
        Choice(
            Action(Literal("a"), "b'\\a'"),
            Action(Literal("b"), "b'\\b'"),
            Action(Literal("n"), "b'\\n'"),
            Action(Literal("f"), "b'\\f'"),
            Action(Literal("r"), "b'\\r'"),
            Action(Literal("t"), "b'\\t'"),
            Action(Literal("v"), "b'\\v'"),
            Action(Literal("\\"), "b'\\\\'"),)),
    # any_char_expr <- "." { AnyChar() }
    RuleDef(
        "any_char_expr",
        Action(
            Literal("."),
            "AnyChar()")),
    # rule_expr <- name:identifier !( __ "<-" ) { Rule(str(name)) }
    RuleDef(
        "rule_expr",
        Action(
            Seq(
                Label(
                    "name",
                    Rule("identifier")),
                Not(
                    Seq(
                        Rule("__"),
                        Literal("<-")))),
            "Rule(str(name))")),

    # char_range_expr <-
    #    '[' content:(
    #        r:class_char_range { [[], [r]] }
    #        / c:class_char { [[str(c)], []] }
    #        / "\\" UnicodeClassEscape )*
    #     ']' ignore:'i'? {
    #         CharRange(
    #             reduce(lambda x, y: x + y, [c[0] for c in content], []),
    #             reduce(lambda x, y: x + y, [c[1] for c in content], []),
    #             )
    #     }
    RuleDef(
        "char_range_expr",
        Action(
            Seq(
                Literal("["),
                Label(
                    "content",
                    Repeat(
                        Choice(
                            Action(
                                Label(
                                    "r",
                                    Rule("class_char_range")),
                                "[[], [r]]"),
                            Action(
                                Label(
                                    "c",
                                    Rule("class_char")),
                                "[[bytes(c)], []]")))),
                Literal("]")),
            tw.dedent("""\
                CharRange(
                    reduce(lambda x, y: x + y, [c[0] for c in content], []),
                    reduce(lambda x, y: x + y, [c[1] for c in content], []),
                )"""))),

    # class_char_range <-
    #     first:class_char '-' second:class_char {[first, second]}
    RuleDef(
        "class_char_range",
        Action(
            Seq(
                Label(
                    "first",
                    Rule("class_char")),
                Literal("-"),
                Label(
                    "second",
                    Rule("class_char"))),
            "[str(first), str(second)]")),

    # class_char <-
    #     (!( "]" / "\\" / EOL ) char:source_char
    #     / "\\" char:char_class_escape) { char }
    RuleDef(
        "class_char",
        Action(
            Choice(
                Seq(
                    Not(
                        Choice(
                            Literal("]"),
                            Literal("\\"),
                            Rule("EOL"))),
                    Label(
                        "char",
                        AnyChar())),
                Seq(
                    Literal("\\"),
                    Label(
                        "char",
                        Rule("char_class_escape")))),
            "char")),

    # char_class_escape <- ']' {"]"} / common_escape
    RuleDef(
        "char_class_escape",
        Choice(
            Action(
                Literal("]"),
                "b']'"),
            Rule("common_escape"))),

    # comment <- "#" ( !EOL source_char )*
    RuleDef(
        "comment",
        Seq(
            Literal("#"),
            Repeat(
                Seq(
                    Not(Rule("EOL")),
                    AnyChar())))),
    # source_char <- .
    # identifier <- $([a-zA-Z_] [a-zA-Z0-9_]*)
    RuleDef(
        "identifier",
        Slice(
            Seq(
                CharRange("_", [["a", "z"], ["A", "Z"]]),
                Repeat(
                    CharRange("_", [["a", "z"], ["A", "Z"], ["0", "9"]]))))),
    # __ <- ( whitespace / EOL / comment )*
    RuleDef(
        "__",
        Repeat(
            Choice(
                Rule("whitespace"),
                Rule("EOL"),
                Rule("comment")))),
    # _ <- whitespace*
    RuleDef(
        "_",
        Repeat(
            Rule("whitespace"))),
    # whitespace <- [ \t\r]
    RuleDef(
        "whitespace",
        CharRange(" \t\r")),
    # EOL <- "\n"
    RuleDef(
        "EOL",
        Literal("\n")),
    # EOS <- _ comment? EOL / __ EOF
    RuleDef(
        "EOS",
        Choice(
            Seq(
                Rule("_"),
                Optional(
                    Rule("comment")),
                Rule("EOL")),
            Seq(
                Rule("__"),
                Rule("EOF")))),
    # EOF <- !.
    RuleDef(
        "EOF",
        Not(AnyChar()))
)

ns = globals()

compile_peg(bootstrapper, ns=ns)

GRAMMAR = r"""
grammar <- ( __ rules:( rule \__ )+ EOF ) { RuleSet(*[r[0] for r in rules]) }

rule <- precedence_climbing / expr_rule

expr_rule <- name:identifier __ "<-" __ expr:expression EOS { RuleDef(str(name), expr) }

precedence_climbing <- name:identifier __ "<-" __
                   levels:(precedence_level \__)+
                   atoms:precedence_atoms
                   { PrecedenceRule(str(name), *([l[0] for l in levels] + atoms)) }

prec_rules_sep <- __ "|" __

precedence_level <- "|--" rules:precedence_rules { rules }

precedence_rules <-
    left_assoc_infix
    / right_assoc_infix
    / prefix_rule
    / postfix_rule


left_assoc_infix <-
    rules:(\prec_rules_sep \"(@)" \__ expression \__ \"@" \__ code_block)+ {
       LeftAssocInfix(None, rules)
    }
right_assoc_infix <-
    rules:(\prec_rules_sep \"@" \__ expression \__ \"(@)" \__ code_block)+ {
       RightAssocInfix(None, rules)
    }
prefix_rule <-
    rules:(\prec_rules_sep expression \__ \"@" \__ code_block)+ {
       Prefix(None, rules)
    }
postfix_rule <-
    rules:(\prec_rules_sep \"@" \__ no_block_expr \__ code_block)+ {
       Postfix(None, rules)
    }
precedence_atoms <-
    "|--" rules:(\__ \"|" \__ expression)+ { [r[0] for r in rules] }

expression <- choice_expr
no_block_expr <- no_block_choice_expr
choice_expr <- ( first:seq_expr rest:( \__ \"/" \__ seq_expr )* ) {
    if not rest:
        return first
    else:
        parts = [first] + [r[0] for r in rest]
        return Choice(*parts) }
no_block_choice_expr <-
    first:no_block_seq_expr rest:( \__ \"/" \__ no_block_seq_expr )* {
        if not rest:
            return first
        else:
            parts = [first] + [r[0] for r in rest]
            return Choice(*parts) }
seq_expr <-
    first:labeled_expr
    rest:( \__ labeled_expr )*
    ( __ action:code_block )? {
        if not rest:
            expr = first
        else:
            parts = [first] + [r[0] for r in rest]
            expr = Seq(*parts)
        if action:
            return Action(expr, tw.dedent(action))
        else:
            return expr }
no_block_seq_expr <-
    first:labeled_expr
    rest:( \__ labeled_expr )* {
        if not rest:
            expr = first
        else:
            parts = [first] + [r[0] for r in rest]
            expr = Seq(*parts)
        return expr }

code_block <- ( "{" code:code "}" ) { code }
code <- code:$( ( ![{}] . )+ / ( "{" code "}" ) )* { str(code) }

labeled_expr <- ( label:($$( identifier \__ \":" \__ ))? expr:prefixed_expr ) {
    if label:
        return Label(str(label), expr)
    else:
        return expr }
prefixed_expr <- ( prefixes:($$( prefix \__ ))* expr:suffixed_expr ) {
    prefix_classes = {
        "\\": Discard,
        "!": Not,
        "&": LookAhead,
        "$": Slice,
        "$$": Concat,
    }
    for p in reversed(prefixes):
        expr = prefix_classes[str(p)](expr)
    expr }
suffixed_expr <- ( expr:primary_expr suffix:($$( \__ suffix ))? ) {
    if not suffix:
        return expr
    suffix_classes = {
        "?": Optional,
        "+": OneOrMore,
        "*": Repeat,
    }
    return suffix_classes[str(suffix)](expr) }
suffix <- [?+*]
prefix <- "$$" / [\\$!&]
primary_expr <-
    rule_expr / lit_expr / char_range_expr / any_char_expr / sub_expr
sub_expr <- ( "(" __ expr:expression __ ")" ) { expr }
lit_expr <- ( lit:string_literal ignore:"i"? ) { Literal(bytes(lit), ignorecase=bool(ignore)) }
string_literal <- ( ( "\"" content:($$double_string_char*) "\"" ) / ( "'" content:($$single_string_char*) "'" ) ) { content }
double_string_char <- ( ( !( "\"" / "\\" / EOL ) char:. ) / ( "\\" char:double_string_escape ) ) { char }
single_string_char <- ( ( !( "'" / "\\" / EOL ) char:. ) / ( "\\" char:single_string_escape ) ) { char }
single_string_escape <- ( "'" { b"'" } ) / common_escape
double_string_escape <- ( "\"" { b'"' } ) / common_escape
common_escape <- single_char_escape
single_char_escape <- ( "a" { b'\a' } ) / ( "b" { b'\b' } ) / ( "n" { b'\n' } ) / ( "f" { b'\f' } ) / ( "r" { b'\r' } ) / ( "t" { b'\t' } ) / ( "v" { b'\v' } ) / ( "\\" { b'\\' } )
any_char_expr <- "." { AnyChar() }
rule_expr <- ( name:identifier !( __ "<-" ) ) { Rule(str(name)) }
char_range_expr <- ( "[" content:( ( r:class_char_range { [[], [r]] } ) / ( c:class_char { [[bytes(c)], []] } ) )* "]" ) {
    CharRange(
        reduce(lambda x, y: x + y, [c[0] for c in content], []),
        reduce(lambda x, y: x + y, [c[1] for c in content], []),
    ) }
class_char_range <- ( first:class_char "-" second:class_char ) { [str(first), str(second)] }
class_char <- ( ( !( "]" / "\\" / EOL ) char:. ) / ( "\\" char:char_class_escape ) ) { char }
char_class_escape <- ( "]" { b']' } ) / common_escape
comment <- "#" ( !EOL . )*
identifier <- $( [_a-zA-Z] [_a-zA-Z0-9]* )
__ <- ( whitespace / EOL / comment )*
_ <- whitespace*
whitespace <- [ \t\r]
EOL <- "\n"
EOS <- ( _ comment? EOL ) / ( __ EOF )
EOF <- !.
"""
parser = bootstrapper(Reader(GRAMMAR))
compile_peg(parser, ns=ns)
